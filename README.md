# Language Flags
![Language Flags](./screenshot.jpg) 

## Description
Language_Flags is a module for Gallery 3 which will replace the default language selection box on the Gallery sidebar with pictures of flags for the available language. The modules comes with three different flag styles, which can be controlled from the admin screen.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "language_flags" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 1.1.3:**
> - Updated module.info file for changes in Gallery 3.0.2.
> - Released 03 August 2011.
>
> Download: [Version 1.1.3](/uploads/c829a103e2bbc06836d503d658d52648/language_flags113.zip)

**Version 1.1.2:**
> - Minor CSS fixes.
> - Released 01 November 2010.
>
> Download: [Version 1.1.2](/uploads/c2de654464d44a589c0d5074fa0e6c3a/language_flags112.zip)

**Version 1.1.1:**
> - Fixed HTML code to make module XHTML 1.0 Transitional compliant.
> - Released 14 June 2010.
>
> Download: [Version 1.1.1](/uploads/4532d0c9f2e01c71bf3b86bb92828e14/language_flags111.zip)

**Version 1.1.0:**
> - Numerous CSS changes (mostly to make it easy to theme).
> - Set a default flag for the module to display if a flag isn't available for a country.
> - Imported more flag pictures.
> - Released 10 March 2010.
>
> Download: [Version 1.1.0](/uploads/16e3141a3d0f4310f34905a51807317f/language_flags110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 04 March 2010.
>
> Download: [Version 1.0.0](/uploads/7d9f70c1c4a90af51051ac31571a58b3/language_flags100.zip)
